USE [HRCloudContacts]
GO
IF(SELECT COUNT(*) FROM [dbo].[Contact])>0 RETURN;
SET IDENTITY_INSERT [dbo].[Contact] ON 
GO
INSERT [dbo].[Contact] ([ContactID], [Name], [Surname], [Adress], [City]) VALUES (1, N'Mirko', N'Filipović', N'Ilica 221', N'Zagreb')
GO
INSERT [dbo].[Contact] ([ContactID], [Name], [Surname], [Adress], [City]) VALUES (2, N'Goran', N'Ivanišević', N'Marmontova 21', N'Split')
GO
INSERT [dbo].[Contact] ([ContactID], [Name], [Surname], [Adress], [City]) VALUES (3, N'Zoran', N'Primorac', N'Vukovarska 23', N'Zagreb')
GO
INSERT [dbo].[Contact] ([ContactID], [Name], [Surname], [Adress], [City]) VALUES (4, N'Josipa', N'Lisac', N'Kvaternikov trg 4', N'Zagreb')
GO
INSERT [dbo].[Contact] ([ContactID], [Name], [Surname], [Adress], [City]) VALUES (6, N'Dino', N'Dvornik', N'Žnjan 23', N'Split')
GO
INSERT [dbo].[Contact] ([ContactID], [Name], [Surname], [Adress], [City]) VALUES (9, N'Frano', N'Gracin', N'Ljerke Šram 6', N'Zagreb')
GO
INSERT [dbo].[Contact] ([ContactID], [Name], [Surname], [Adress], [City]) VALUES (10, N'Josip', N'Jelačić', N'šuma', N'Zaprešić')
GO
INSERT [dbo].[Contact] ([ContactID], [Name], [Surname], [Adress], [City]) VALUES (11, N'Nikola', N'Tesla', N'Smiljanska 23', N'Smiljani')
GO
INSERT [dbo].[Contact] ([ContactID], [Name], [Surname], [Adress], [City]) VALUES (12, N'Marko', N'Marulić', NULL, NULL)
GO
INSERT [dbo].[Contact] ([ContactID], [Name], [Surname], [Adress], [City]) VALUES (13, N'Milan', N'Šufflay', NULL, NULL)
GO
INSERT [dbo].[Contact] ([ContactID], [Name], [Surname], [Adress], [City]) VALUES (14, N'Petar', N'Hektorović', N'Tvardoj 2', N'Stari Grad')
GO
INSERT [dbo].[Contact] ([ContactID], [Name], [Surname], [Adress], [City]) VALUES (15, N'Grgur', N'Ninski', NULL, NULL)
GO
INSERT [dbo].[Contact] ([ContactID], [Name], [Surname], [Adress], [City]) VALUES (16, N'Šiško', N'Menčetić', NULL, NULL)
GO
INSERT [dbo].[Contact] ([ContactID], [Name], [Surname], [Adress], [City]) VALUES (18, N'Petar', N'Berislavić', NULL, NULL)
GO
INSERT [dbo].[Contact] ([ContactID], [Name], [Surname], [Adress], [City]) VALUES (20, N'Marin', N'Držić', N'Stradun 23', N'Dubrovnik')
GO
INSERT [dbo].[Contact] ([ContactID], [Name], [Surname], [Adress], [City]) VALUES (21, N'Anakin', N'Skywalker', N'Star way 2', N'Pluto')
GO
SET IDENTITY_INSERT [dbo].[Contact] OFF
GO
IF(SELECT COUNT(*) FROM [dbo].[Email])>0 RETURN;
SET IDENTITY_INSERT [dbo].[Email] ON 

GO
INSERT [dbo].[Email] ([EmailID], [Adress], [ContactID]) VALUES (3, N'mirko@filipovic@org', 1)
GO
INSERT [dbo].[Email] ([EmailID], [Adress], [ContactID]) VALUES (4, N'goran.ivanisevic@gmail.com', 2)
GO
INSERT [dbo].[Email] ([EmailID], [Adress], [ContactID]) VALUES (6, N'zoki@t-ht.zg.t-com.de.hr', 3)
GO
INSERT [dbo].[Email] ([EmailID], [Adress], [ContactID]) VALUES (7, N'mirko5@gmail.com', 1)
GO
INSERT [dbo].[Email] ([EmailID], [Adress], [ContactID]) VALUES (8, N'mirko@sdp.hr', 1)
GO
INSERT [dbo].[Email] ([EmailID], [Adress], [ContactID]) VALUES (11, N'fsf@fhjdf', 6)
GO
INSERT [dbo].[Email] ([EmailID], [Adress], [ContactID]) VALUES (12, N'fsf@fhjdf2', 6)
GO
INSERT [dbo].[Email] ([EmailID], [Adress], [ContactID]) VALUES (13, N'fsf@fhjdf3', 6)
GO
INSERT [dbo].[Email] ([EmailID], [Adress], [ContactID]) VALUES (17, N'frano.gracin@gmail', 9)
GO
INSERT [dbo].[Email] ([EmailID], [Adress], [ContactID]) VALUES (18, N'franogg@gmail.com', 9)
GO
INSERT [dbo].[Email] ([EmailID], [Adress], [ContactID]) VALUES (19, N'', 10)
GO
INSERT [dbo].[Email] ([EmailID], [Adress], [ContactID]) VALUES (20, N'', 11)
GO
INSERT [dbo].[Email] ([EmailID], [Adress], [ContactID]) VALUES (21, N'', 12)
GO
INSERT [dbo].[Email] ([EmailID], [Adress], [ContactID]) VALUES (22, N'', 13)
GO
INSERT [dbo].[Email] ([EmailID], [Adress], [ContactID]) VALUES (23, N'', 14)
GO
INSERT [dbo].[Email] ([EmailID], [Adress], [ContactID]) VALUES (24, N'', 15)
GO
INSERT [dbo].[Email] ([EmailID], [Adress], [ContactID]) VALUES (25, N'', 16)
GO
INSERT [dbo].[Email] ([EmailID], [Adress], [ContactID]) VALUES (27, N'', 18)
GO
INSERT [dbo].[Email] ([EmailID], [Adress], [ContactID]) VALUES (30, N'marin@gmail.comEDIT', 20)
GO
INSERT [dbo].[Email] ([EmailID], [Adress], [ContactID]) VALUES (32, N'marinko@gmail.hr', 20)
GO
INSERT [dbo].[Email] ([EmailID], [Adress], [ContactID]) VALUES (33, N'anakin@force.org', 21)
GO
SET IDENTITY_INSERT [dbo].[Email] OFF
GO
IF(SELECT COUNT(*) FROM [dbo].[PhoneNumber])>0 RETURN;
SET IDENTITY_INSERT [dbo].[PhoneNumber] ON 

GO
INSERT [dbo].[PhoneNumber] ([PhoneNumberID], [Number], [ContactID]) VALUES (1, N'098123895', 1)
GO
INSERT [dbo].[PhoneNumber] ([PhoneNumberID], [Number], [ContactID]) VALUES (2, N'099345784', 2)
GO
INSERT [dbo].[PhoneNumber] ([PhoneNumberID], [Number], [ContactID]) VALUES (3, N'+3851254789', 1)
GO
INSERT [dbo].[PhoneNumber] ([PhoneNumberID], [Number], [ContactID]) VALUES (4, N'0919785132', 3)
GO
INSERT [dbo].[PhoneNumber] ([PhoneNumberID], [Number], [ContactID]) VALUES (5, N'0910978643', 4)
GO
INSERT [dbo].[PhoneNumber] ([PhoneNumberID], [Number], [ContactID]) VALUES (7, N'489046', 6)
GO
INSERT [dbo].[PhoneNumber] ([PhoneNumberID], [Number], [ContactID]) VALUES (8, N'34534', 6)
GO
INSERT [dbo].[PhoneNumber] ([PhoneNumberID], [Number], [ContactID]) VALUES (13, N'0998214920', 9)
GO
INSERT [dbo].[PhoneNumber] ([PhoneNumberID], [Number], [ContactID]) VALUES (14, N'+68516115349', 9)
GO
INSERT [dbo].[PhoneNumber] ([PhoneNumberID], [Number], [ContactID]) VALUES (15, N'3851123456', 10)
GO
INSERT [dbo].[PhoneNumber] ([PhoneNumberID], [Number], [ContactID]) VALUES (16, N'01111000', 11)
GO
INSERT [dbo].[PhoneNumber] ([PhoneNumberID], [Number], [ContactID]) VALUES (17, N'niman', 12)
GO
INSERT [dbo].[PhoneNumber] ([PhoneNumberID], [Number], [ContactID]) VALUES (18, N'nijemam', 13)
GO
INSERT [dbo].[PhoneNumber] ([PhoneNumberID], [Number], [ContactID]) VALUES (19, N'021765021', 14)
GO
INSERT [dbo].[PhoneNumber] ([PhoneNumberID], [Number], [ContactID]) VALUES (20, N'nimam', 15)
GO
INSERT [dbo].[PhoneNumber] ([PhoneNumberID], [Number], [ContactID]) VALUES (21, N'nijemam', 16)
GO
INSERT [dbo].[PhoneNumber] ([PhoneNumberID], [Number], [ContactID]) VALUES (23, N'123456', 18)
GO
INSERT [dbo].[PhoneNumber] ([PhoneNumberID], [Number], [ContactID]) VALUES (25, N'0284561e', 20)
GO
INSERT [dbo].[PhoneNumber] ([PhoneNumberID], [Number], [ContactID]) VALUES (26, N'028456189', 20)
GO
INSERT [dbo].[PhoneNumber] ([PhoneNumberID], [Number], [ContactID]) VALUES (30, N'new234', 20)
GO
INSERT [dbo].[PhoneNumber] ([PhoneNumberID], [Number], [ContactID]) VALUES (31, N'019895656', 21)
GO
SET IDENTITY_INSERT [dbo].[PhoneNumber] OFF
GO
IF(SELECT COUNT(*) FROM [dbo].[Tag])>0 RETURN;
SET IDENTITY_INSERT [dbo].[Tag] ON 

GO
INSERT [dbo].[Tag] ([TagID], [TagText]) VALUES (1, N'sport')
GO
INSERT [dbo].[Tag] ([TagID], [TagText]) VALUES (2, N'glazba')
GO
INSERT [dbo].[Tag] ([TagID], [TagText]) VALUES (3, N'SDP')
GO
INSERT [dbo].[Tag] ([TagID], [TagText]) VALUES (4, N'tenis')
GO
INSERT [dbo].[Tag] ([TagID], [TagText]) VALUES (5, N'split')
GO
INSERT [dbo].[Tag] ([TagID], [TagText]) VALUES (6, N'music')
GO
INSERT [dbo].[Tag] ([TagID], [TagText]) VALUES (7, N'funk')
GO
INSERT [dbo].[Tag] ([TagID], [TagText]) VALUES (8, N'animacija')
GO
INSERT [dbo].[Tag] ([TagID], [TagText]) VALUES (9, N'rer')
GO
INSERT [dbo].[Tag] ([TagID], [TagText]) VALUES (10, N'erteze')
GO
INSERT [dbo].[Tag] ([TagID], [TagText]) VALUES (11, N'tdf')
GO
INSERT [dbo].[Tag] ([TagID], [TagText]) VALUES (12, N'dfh')
GO
INSERT [dbo].[Tag] ([TagID], [TagText]) VALUES (13, N'dev')
GO
INSERT [dbo].[Tag] ([TagID], [TagText]) VALUES (14, N'zagreb')
GO
INSERT [dbo].[Tag] ([TagID], [TagText]) VALUES (15, N'povijest')
GO
INSERT [dbo].[Tag] ([TagID], [TagText]) VALUES (16, N'')
GO
INSERT [dbo].[Tag] ([TagID], [TagText]) VALUES (17, N'IT')
GO
INSERT [dbo].[Tag] ([TagID], [TagText]) VALUES (18, N'znanost')
GO
INSERT [dbo].[Tag] ([TagID], [TagText]) VALUES (19, N'HR')
GO
INSERT [dbo].[Tag] ([TagID], [TagText]) VALUES (20, N'2')
GO
INSERT [dbo].[Tag] ([TagID], [TagText]) VALUES (21, N'književnost')
GO
INSERT [dbo].[Tag] ([TagID], [TagText]) VALUES (22, N'marulologija')
GO
INSERT [dbo].[Tag] ([TagID], [TagText]) VALUES (23, N'SW')
GO
SET IDENTITY_INSERT [dbo].[Tag] OFF
GO
IF(SELECT COUNT(*) FROM [dbo].[ContactTag])>0 RETURN;
SET IDENTITY_INSERT [dbo].[ContactTag] ON 

GO
INSERT [dbo].[ContactTag] ([ContactTagID], [ContactID], [TagID]) VALUES (1, 1, 1)
GO
INSERT [dbo].[ContactTag] ([ContactTagID], [ContactID], [TagID]) VALUES (2, 1, 3)
GO
INSERT [dbo].[ContactTag] ([ContactTagID], [ContactID], [TagID]) VALUES (3, 2, 1)
GO
INSERT [dbo].[ContactTag] ([ContactTagID], [ContactID], [TagID]) VALUES (4, 2, 4)
GO
INSERT [dbo].[ContactTag] ([ContactTagID], [ContactID], [TagID]) VALUES (5, 3, 1)
GO
INSERT [dbo].[ContactTag] ([ContactTagID], [ContactID], [TagID]) VALUES (6, 3, 4)
GO
INSERT [dbo].[ContactTag] ([ContactTagID], [ContactID], [TagID]) VALUES (7, 4, 2)
GO
INSERT [dbo].[ContactTag] ([ContactTagID], [ContactID], [TagID]) VALUES (8, 6, 5)
GO
INSERT [dbo].[ContactTag] ([ContactTagID], [ContactID], [TagID]) VALUES (9, 6, 6)
GO
INSERT [dbo].[ContactTag] ([ContactTagID], [ContactID], [TagID]) VALUES (10, 6, 7)
GO
INSERT [dbo].[ContactTag] ([ContactTagID], [ContactID], [TagID]) VALUES (14, 9, 2)
GO
INSERT [dbo].[ContactTag] ([ContactTagID], [ContactID], [TagID]) VALUES (15, 9, 13)
GO
INSERT [dbo].[ContactTag] ([ContactTagID], [ContactID], [TagID]) VALUES (16, 9, 14)
GO
INSERT [dbo].[ContactTag] ([ContactTagID], [ContactID], [TagID]) VALUES (17, 9, 6)
GO
INSERT [dbo].[ContactTag] ([ContactTagID], [ContactID], [TagID]) VALUES (18, 10, 15)
GO
INSERT [dbo].[ContactTag] ([ContactTagID], [ContactID], [TagID]) VALUES (19, 10, 16)
GO
INSERT [dbo].[ContactTag] ([ContactTagID], [ContactID], [TagID]) VALUES (20, 11, 17)
GO
INSERT [dbo].[ContactTag] ([ContactTagID], [ContactID], [TagID]) VALUES (21, 11, 18)
GO
INSERT [dbo].[ContactTag] ([ContactTagID], [ContactID], [TagID]) VALUES (23, 13, 19)
GO
INSERT [dbo].[ContactTag] ([ContactTagID], [ContactID], [TagID]) VALUES (24, 14, 16)
GO
INSERT [dbo].[ContactTag] ([ContactTagID], [ContactID], [TagID]) VALUES (25, 15, 16)
GO
INSERT [dbo].[ContactTag] ([ContactTagID], [ContactID], [TagID]) VALUES (26, 16, 16)
GO
INSERT [dbo].[ContactTag] ([ContactTagID], [ContactID], [TagID]) VALUES (28, 18, 16)
GO
INSERT [dbo].[ContactTag] ([ContactTagID], [ContactID], [TagID]) VALUES (30, 20, 21)
GO
INSERT [dbo].[ContactTag] ([ContactTagID], [ContactID], [TagID]) VALUES (33, 12, 21)
GO
INSERT [dbo].[ContactTag] ([ContactTagID], [ContactID], [TagID]) VALUES (34, 12, 5)
GO
INSERT [dbo].[ContactTag] ([ContactTagID], [ContactID], [TagID]) VALUES (35, 12, 22)
GO
SET IDENTITY_INSERT [dbo].[ContactTag] OFF
GO
