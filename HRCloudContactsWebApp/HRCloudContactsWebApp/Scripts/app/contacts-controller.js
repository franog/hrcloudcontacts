﻿var mainApp = angular.module('ContactsApp',['ngRoute']);


mainApp.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.
    when('/Contacts/Add', {
        templateUrl: 'Home/ContactAddEdit',
        controller: 'ContactAddCtrl'
    }).
    when('/Contacts', {
        templateUrl: 'Home/ContactList',
        controller: 'ContactsCtrl'
    }).
    when('/Contacts/:contactID', {
        templateUrl: 'Home/ContactAddEdit',
        controller: 'ContactEditCtrl'
    }).
    when('/About', {
        templateUrl: 'Home/About',
        controller: 'AboutController'
    }).
    otherwise({
        templateUrl: 'Home/ContactList',
        controller: 'ContactsCtrl'
    })
    ;
}]);

mainApp.factory('statusService', function () {
    var statusMessage = ''
    function set(data) {
        statusMessage = data;
    }
    function get() {
        return statusMessage;
    }
    return {
        set: set,
        get: get
    }
});

mainApp.controller('ContactsCtrl', function ($scope, $http, statusService) {
    $scope.allContacts = function () {
        $scope.title = "loading contacts...";
        $scope.statusMessage = statusService.get();
        if ($scope.statusMessage != '') statusService.set('');
        $scope.contacts = [];


        $http.get("/api/APIContacts").success(function (data, status, headers, config) {
            $scope.contacts = data.contacts;
            $scope.title = "Contacts";

    }).error(function (data, status, headers, config) {
        statusService.set('Oops... something went wrong');
    });
    };
    $scope.FindContacts = function () {
        if ($scope.searchInput.trim() == '') $scope.allContacts();
        if ($scope.searchInput.trim().length < 2) return;
        $scope.statusMessage = statusService.get();
        if ($scope.statusMessage != '') statusService.set('');
 
        $http.get('/api/APIContacts/search/'+$scope.searchInput).success(function (data, status, headers, config) {
            $scope.contacts = data.contacts;

        }).error(function (data, status, headers, config) {
            statusService.set('Oops... something went wrong');
        });
    };
    $scope.DeleteContact = function (index) {
        if (confirm("Are you sure you want to delete contact?")) {
            $http.delete('/api/APIContacts/' + index).success(function (response) {
                statusService.set('Contact deleted.');
                $scope.allContacts();
            }).error(function (response) {
                statusService.set('Oops... something went wrong');
            });
        }
    };
});

mainApp.controller('AboutController', function ($scope, $http) {
    $scope.title = "About";
});

mainApp.controller('ContactAddCtrl', function ($scope, $http, $window, statusService) {
    $scope.title = "Add contact";
    $scope.submitText = "Save new contact";
    $scope.emails = [{ adress: '' }]
    $scope.phoneNumbers = [{ number: '' }]
    $scope.tags = [{ tagText: '' }]

    $scope.AddPhoneNumber = function () {
        $scope.phoneNumbers.push({ adress: '' });
    }
    $scope.AddEmail = function () {
        $scope.emails.push({ adress: '' });
    }
    $scope.AddTag = function () {
        $scope.tags.push({ tagText: '' });
    }
    $scope.SaveContact = function () {
        var tagCount = $scope.tags;

        var contact = {
            name:$scope.name,
            surname: $scope.surname,
            adress: $scope.adress,
            city: $scope.city,
            emails: $scope.emails,
            phoneNumbers: $scope.phoneNumbers,
            tags: $scope.tags
        }
        $http.post(
                '/api/APIContacts',
                JSON.stringify(contact),
                {
                    headers: { 'Content-Type': 'application/json' }
                }
            ).success(function (data, status, headers, config) {
            $window.location.href = '#/Contacts';
            statusService.set('Contact saved.');

        }).error(function (data, status, headers, config) {
            $scope.title = "Oops... something went wrong";
        });
    };
});


mainApp.controller('ContactEditCtrl', function ($scope, $http, $window, statusService, $routeParams) {
    $scope.title = "Edit contact";
    $scope.submitText = "Save changes";
    $scope.contactID = $routeParams.contactID

    $http.get("/api/APIContacts/" + $scope.contactID).success(function (data, status, headers, config) {
        $scope.name = data.name;
        $scope.surname = data.surname;
        $scope.adress = data.adress;
        $scope.city = data.city;
        $scope.emails = data.emails;
        $scope.phoneNumbers = data.phoneNumbers;
        $scope.tags = data.tags;

    }).error(function (data, status, headers, config) {
        $scope.title = "Oops... something went wrong";
    });

    $scope.AddPhoneNumber = function () {
        $scope.phoneNumbers.push({ adress: '' });
    }
    $scope.AddEmail = function () {
        $scope.emails.push({ adress: '' });
    }
    $scope.AddTag = function () {
        $scope.tags.push({ tagText: '' });
    }
    $scope.SaveContact = function () {
         var contact = {
            contactID: $scope.contactID,
            name: $scope.name,
            surname: $scope.surname,
            adress: $scope.adress,
            city: $scope.city,
            emails: $scope.emails,
            phoneNumbers: $scope.phoneNumbers,
            tags: $scope.tags,
            propertiesDirty:$scope.propertiesDirty
         }
        $http.put(
                '/api/APIContacts/' + contact.contactID,
                JSON.stringify(contact),
                {
                    headers: { 'Content-Type': 'application/json' }
                }
            ).success(function (data) {
                $window.location.href = '#/Contacts';
                statusService.set('Contact saved.');

            }).error(function (data, status, headers, config) {
                $scope.title = "Oops... something went wrong";
            });
    };
});
