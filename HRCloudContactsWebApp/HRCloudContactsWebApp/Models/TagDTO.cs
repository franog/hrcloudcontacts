﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRCloudContactsWebApp.Models
{
    public class TagDTO
    {
        public int TagID { get; set; }
        public string TagText { get; set; }
        public int ContactID { get; set; }
        public bool isDirty = false;
    }
}