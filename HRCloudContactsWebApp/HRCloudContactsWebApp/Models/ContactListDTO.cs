﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRCloudContactsWebApp.Models
{
    public class ContactListDTO
    {
        public ContactListDTO(IQueryable<ContactDTO> contactList)
        {
            Contacts = contactList.ToList();
        }
        public List<ContactDTO> Contacts = new List<ContactDTO>();
    }
}