﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRCloudContactsWebApp.Models
{
    public class ContactDTO : Contact
    {
        public new IEnumerable<EmailDTO> Emails { get; set; }
        [EnsureOneElementAttribute(ErrorMessage ="At least one phone number required.")]
        public new IEnumerable<PhoneNumberDTO> PhoneNumbers { get; set; }
        public IEnumerable<TagDTO> Tags { get; set; }
        public bool propertiesDirty = false;
        public bool IsDirty()
        {
            if (propertiesDirty || EmailsDirty() || PhoneNumbersDirty() || TagsDirty())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool EmailsDirty()
        {
            return Emails.Any(e => e.isDirty);
        }
        public bool PhoneNumbersDirty()
        {
            return PhoneNumbers.Any(p => p.isDirty);
        }
        public bool TagsDirty()
        {
            return Tags.Any(t => t.isDirty);
        }
    }
}