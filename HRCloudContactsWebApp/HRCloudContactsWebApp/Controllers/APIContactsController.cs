﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using HRCloudContactsWebApp.Models;
using System.Data.Entity.Validation;

namespace HRCloudContactsWebApp.Controllers
{
    public class APIContactsController : ApiController
    {
        private HRCloudContactsEntities db = new HRCloudContactsEntities();

        // GET: api/APIContacts
        [ResponseType(typeof(ContactListDTO))]
        public ContactListDTO GetContacts()
        {
            var contactsApiModel = from c in db.Contacts
                                   select new ContactDTO
                                   {
                                       ContactID = c.ContactID,
                                       Name = c.Name,
                                       Surname = c.Surname,
                                       Adress = c.Adress,
                                       City = c.City,
                                       Emails = from e in c.Emails
                                                select new EmailDTO
                                                {
                                                    EmailID = e.EmailID,
                                                    Adress = e.Adress,
                                                    ContactID = e.ContactID
                                                },
                                       PhoneNumbers = from p in c.PhoneNumbers
                                                      select new PhoneNumberDTO
                                                      {
                                                          PhoneNumberID = p.PhoneNumberID,
                                                          Number = p.Number,
                                                          ContactID = p.ContactID
                                                      },
                                       Tags = from t in db.Tags
                                              join ct in c.ContactTags on t.TagID equals ct.TagID
                                              select new TagDTO
                                              {
                                                  TagID = t.TagID,
                                                  TagText = t.TagText,
                                                  ContactID = ct.ContactID
                                              }
                                   };
            return new ContactListDTO(contactsApiModel);
        }       

        // GET: api/APIContacts/5
        [ResponseType(typeof(ContactDTO))]
        //[Route("api/APIContacts/{id:int}")]
        public async Task<IHttpActionResult> GetContact(int id)
        {
            ContactDTO contactModel = await (from c in db.Contacts
                                         where c.ContactID == id
                                             select new ContactDTO
                                             {
                                                 ContactID = c.ContactID,
                                                 Name = c.Name,
                                                 Surname = c.Surname,
                                                 Adress = c.Adress,
                                                 City = c.City,
                                                 Emails = from e in c.Emails
                                                          select new EmailDTO
                                                          {
                                                              EmailID = e.EmailID,
                                                              Adress = e.Adress,
                                                              ContactID = e.ContactID
                                                          },
                                                 PhoneNumbers = from p in c.PhoneNumbers
                                                                select new PhoneNumberDTO
                                                                {
                                                                    PhoneNumberID = p.PhoneNumberID,
                                                                    Number = p.Number,
                                                                    ContactID = p.ContactID
                                                                },
                                                 Tags = from t in db.Tags
                                                        join ct in c.ContactTags on t.TagID equals ct.TagID
                                                        select new TagDTO
                                                        {
                                                            TagID = t.TagID,
                                                            TagText = t.TagText,
                                                            ContactID = ct.ContactID
                                                        }
                                             }).FirstOrDefaultAsync();
            if (contactModel == null)
            {
                return NotFound();
            }

            return Ok(contactModel);
        }

        [ResponseType(typeof(ContactListDTO))]
        [Route("api/APIContacts/search/{searchInput}")]
        public ContactListDTO GetContacts(string searchInput)
        {
            var contactsApiModel = from c in db.Contacts
                                   select new ContactDTO
                                   {
                                       ContactID = c.ContactID,
                                       Name = c.Name,
                                       Surname = c.Surname,
                                       Adress = c.Adress,
                                       City = c.City,
                                       Emails = from e in c.Emails
                                                select new EmailDTO
                                                {
                                                    EmailID = e.EmailID,
                                                    Adress = e.Adress,
                                                    ContactID = e.ContactID
                                                },
                                       PhoneNumbers = from p in c.PhoneNumbers
                                                      select new PhoneNumberDTO
                                                      {
                                                          PhoneNumberID = p.PhoneNumberID,
                                                          Number = p.Number,
                                                          ContactID = p.ContactID
                                                      },
                                       Tags = from t in db.Tags
                                              join ct in c.ContactTags on t.TagID equals ct.TagID
                                              select new TagDTO
                                              {
                                                  TagID = t.TagID,
                                                  TagText = t.TagText,
                                                  ContactID = ct.ContactID
                                              }
                                   };
            var searchContactsApiModel = contactsApiModel.Where(c => c.Tags.Any(t => t.TagText.StartsWith(searchInput)) || c.Name.StartsWith(searchInput) || c.Surname.StartsWith(searchInput));
            return new ContactListDTO(searchContactsApiModel);

        }

        // PUT: api/APIContacts/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutContact(int id, ContactDTO editedContact)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != editedContact.ContactID)
            {
                return BadRequest();
            }
            if (!editedContact.IsDirty())
            {
                return StatusCode(HttpStatusCode.Accepted);
            }

            //db.Entry(editedContact).State = EntityState.Modified;
            var DBContact = db.Contacts.Find(editedContact.ContactID);
            //DBContact.ContactID = editedContact.ContactID;
            DBContact.Adress  = editedContact.Adress;
            DBContact.City = editedContact.City;
            DBContact.Name = editedContact.Name;
            DBContact.Surname = editedContact.Surname;
            if(editedContact.EmailsDirty())
            {
                foreach (Email originalEmail in DBContact.Emails.ToList())
                {
                    Email sameEditedEmail = editedContact.Emails.FirstOrDefault(p => p.EmailID == originalEmail.EmailID);
                    if (sameEditedEmail == null || String.IsNullOrWhiteSpace(sameEditedEmail.Adress))
                    {
                        DBContact.Emails.Remove(originalEmail);
                        db.Entry(originalEmail).State = EntityState.Deleted;
                    }
                    else
                    {
                        originalEmail.Adress = editedContact.Emails.FirstOrDefault(p => p.EmailID == originalEmail.EmailID).Adress;
                    }
                }
                foreach (Email editEmail in editedContact.Emails.Where(p => !String.IsNullOrWhiteSpace(p.Adress) && p.ContactID == 0)
                                                                    .Select(p => new Email { Adress = p.Adress, ContactID = id }).ToList())
                {
                    db.Emails.Add(editEmail);
                    DBContact.Emails.Add(editEmail);
                }
            }
            if (editedContact.PhoneNumbersDirty())
            {
                foreach(PhoneNumber originalPhone in DBContact.PhoneNumbers.ToList())
                {
                    PhoneNumber sameEditedPhone = editedContact.PhoneNumbers.FirstOrDefault(p => p.PhoneNumberID == originalPhone.PhoneNumberID);
                    if (sameEditedPhone==null || String.IsNullOrWhiteSpace(sameEditedPhone.Number))
                    {
                        DBContact.PhoneNumbers.Remove(originalPhone);
                        db.Entry(originalPhone).State = EntityState.Deleted;
                    }
                    else
                    {
                        originalPhone.Number = editedContact.PhoneNumbers.FirstOrDefault(p => p.PhoneNumberID == originalPhone.PhoneNumberID).Number;
                    }
                }
                foreach (PhoneNumber editPhone in editedContact.PhoneNumbers.Where(p => !String.IsNullOrWhiteSpace(p.Number) && p.ContactID == 0)
                                                                    .Select(p => new PhoneNumber { Number = p.Number, ContactID = id}).ToList())
                {
                    db.PhoneNumbers.Add(editPhone);
                    DBContact.PhoneNumbers.Add(editPhone);        
                }
            }

            if (editedContact.TagsDirty())
            {
                foreach(ContactTag cTag in DBContact.ContactTags.ToList())
                    db.Entry(cTag).State = EntityState.Deleted;
                DBContact.ContactTags.Clear();
                foreach (TagDTO tag in editedContact.Tags.Where(t => !String.IsNullOrWhiteSpace(t.TagText)))
                {
                    if (db.Tags.Local.Any(tl => tl.TagText == tag.TagText)) continue;
                    Tag existingTag = db.Tags.FirstOrDefault(t => t.TagText.Equals(tag.TagText, StringComparison.CurrentCultureIgnoreCase));
                    if (existingTag == null)
                    {
                        Tag newTag = new Tag();
                        newTag.TagText = tag.TagText;
                        db.Tags.Add(newTag);
                        DBContact.ContactTags.Add(new ContactTag { Tag = newTag, ContactID = id });
                    }
                    else
                    {
                        DBContact.ContactTags.Add(new ContactTag { Tag = existingTag, ContactID = id });
                    }
                }
            }
            try
            {
                db.SaveChanges();
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ContactExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            catch (DbEntityValidationException )
            {
                return BadRequest(ModelState);
            }
            catch
            {
                return StatusCode(HttpStatusCode.InternalServerError);
            }

            return StatusCode(HttpStatusCode.OK);
        }

        // POST: api/APIContacts
        [ResponseType(typeof(ContactDTO))]
        public async Task<IHttpActionResult> PostContact(ContactDTO contact)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Contact newContact = new Contact();
            newContact.Adress = contact.Adress;
            newContact.City = contact.City;
            newContact.Emails = contact.Emails.Where(e => !String.IsNullOrWhiteSpace(e.Adress)).Select(e=>new Email {Adress=e.Adress}).ToList();
            newContact.PhoneNumbers = contact.PhoneNumbers.Where(p => !String.IsNullOrWhiteSpace(p.Number)).Select(p=>new PhoneNumber { Number = p.Number }).ToList();
            newContact.Name = contact.Name;
            newContact.Surname = contact.Surname;
            foreach (TagDTO tag in contact.Tags.Where(t=>!t.TagText.Trim().Equals(string.Empty)))
            {
                Tag existingTag = db.Tags.FirstOrDefault(t => t.TagText.Equals(tag.TagText, StringComparison.CurrentCultureIgnoreCase));
                if (existingTag == null)
                {
                    Tag newTag = new Tag();
                    newTag.TagText = tag.TagText;
                    db.Tags.Add(newTag);
                    db.SaveChanges();
                    newContact.ContactTags.Add(new ContactTag { TagID = newTag.TagID });
                }
                else
                {
                    newContact.ContactTags.Add(new ContactTag { TagID = existingTag.TagID });
                }
            }
            db.Contacts.Add(newContact);
            await db.SaveChangesAsync();

            return Ok();
        }

        // DELETE: api/APIContacts/5
        [ResponseType(typeof(ContactDTO))]
        public async Task<IHttpActionResult> DeleteContact(int id)
        {
            Contact contact = await db.Contacts.FindAsync(id);
            if (contact == null)
            {
                return NotFound();
            }
            try
            {
            foreach (Email email in contact.Emails.ToList()) db.Emails.Remove(email);
            foreach (PhoneNumber phone in contact.PhoneNumbers.ToList()) db.PhoneNumbers.Remove(phone);
            foreach (ContactTag contactTag in contact.ContactTags.ToList()) db.ContactTags.Remove(contactTag);
            db.Contacts.Remove(contact);
            await db.SaveChangesAsync();
            }
            catch(Exception ex)
            {
                return InternalServerError(ex);
            }

            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ContactExists(int id)
        {
            return db.Contacts.Count(e => e.ContactID == id) > 0;
        }
    }
}